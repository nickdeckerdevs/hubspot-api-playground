<html>
<head>
  <title>something about a concept</title>
  <style>
    body {
      font: normal normal 18px/1.5 Arial, Helvetica, sans-serif;
    }
  </style>
</head>
<body>
<?php
require __DIR__ . '/vendor/autoload.php';

$style_for_hs_pages = '<style>section .row-fluid [class="span6"]:first-child { display: none; }'
  . 'body { font: normal normal 18px/1.5 Arial, Helvetica, sans-serif; }'
  . 'section .row-fluid [class="span6"]:last-child { width: 100%; }'
  . 'h1 { display: none; }'
  . '#hs_cos_wrapper_subheader { font: small-caps 400 2.5em/1.25 Arial, Helvetica, sans-serif; }'
  . '</style>';
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
  $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
  $ip = $_SERVER['REMOTE_ADDR'];
}
if(isset($_GET) && isset($_GET['url'])) {
  $url = $_GET['url'];
  $wordpress_pages = [ $url ];
} else {
?>
<pre>
/*
 * This is a better implementation that is extendable and object oriented
 * This setup allows you to get process an array of urls and take the:
 *            &lt;article&gt;&lt;/article&gt;
 * and upload them into hubspot. The current setup here is a proof of concept
 * using the HubSpot API and uploading to the Demo Hubspot Account.
 * You can see the previous implementation in the repo or through another link
 * After the three wordpress pages are uploaded you are able to paste your own
 * wordpress page url and see how it goes into hubspot.
 * Please note this is just a general template that we modify slightly.
 *
 * Issues:
 * - Internal and external links: how to manage
 * - Images without http: Probably need to check SRC as the regex
 * -
 */
</pre>
<h1>proof of concept</h1>

<?php
  $wordpress_pages = [
    'http://www.relittle.com/walk-behind-riding-mowers/',
    'http://info.avalan.com/shop/aw900xtp-pair-900-mhz-outdoor-ethernet-panel-bridge/',
    'https://www.kaocollins.com/inktank/category/inspiration/'
  ];
}

foreach($wordpress_pages as $key => $url) {
  echo "Current url: $url<br>";
  $page = new Webpage($url);
  $downloaded_images_array = $page->download_images();
  echo "View page details that we parsed:<br>";
  d($page);
  $hub_images = new Hubspot();
  $new_images_array = $hub_images->upload_images($downloaded_images_array);
  $page->replace_images_with_new($new_images_array);
  $test_slug = $page->slug . '-' . rand(5, 1291223);
  $body = [
    'name' => $page->title,
    'meta_description' => $page->meta,
    'template_path' => 'hubspot_default/landing_page/basic_with_form/2_col_form_left.html',
    'slug' => $test_slug,
    'html_title' => $page->title,
    'publish_immediately' => true,
    'head_html' => $style_for_hs_pages,
    'widgets' => [
      'companybyline' => [
        'value' => 'nickdeckerdevs.com'
      ],
      'subheader' => [
        'body' => [
          'value' => $page->headline
        ]
      ],
      'right_column' => [
        'body' => [
          'html' => $page->content
        ]
      ]
    ]
  ];

  $page_params = [
    'body' => $body,
    'publish' => true
  ];
  d($page_params);
  $hubapi = new Hubspot();
  $new_page = $hubapi->create_new_website_page($page_params);
  $myfile = file_put_contents('urls.txt', $ip.','.urldecode($url).','.$new_page.','.PHP_EOL , FILE_APPEND | LOCK_EX);
}
?>
<div style="width: 450px; margin: 100px auto 0 auto; background: #efefef; padding: 20px;">
  <form action="/wordpress-test.php" method="get">
    <label style="text-align: center;">Enter your own wordpress page url:
      <input type="url" name="url" style="width: 80%; margin: 20px 10%; font-size: 1.5em; line-height: 1.5;" placeholder="Paste A Wordpress URL here...">
    </label>
    <div style="text-align: center;padding: 20px 20px 0 20px;">
      <input type="submit" name="submit" value="Create New Hubspot Page" style="font-size: 1.5em;">
    </div>
  </form>
</div>
<?php include_once('footer-includes.php'); ?>
</body>
</html>

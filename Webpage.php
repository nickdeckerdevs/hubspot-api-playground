<?php
use \Curl\Curl;


class Webpage {
  var $url;
  var $html;
  var $slug;
  var $title;
  var $meta;
  var $content;
  var $headline;
  var $images = [];
  var $imagenames = [];

  function __construct($url) {
    $this->url = $url;
    $this->get_page();
    echo "Page [{$this->title}] created.<br>";
  }

  function get_page() {
    $this->set_html();
    $this->set_content();
    $this->set_headline();
    $this->set_title();
    if($this->title == '' || $this->title == ' ') {
      $this->title = $this->headline;
    }
    $this->set_meta_description();
    $this->set_slug();
  }

  /* overwrites content in current object */
  function set_url($new_url) {
    $this->url = $new_url;
    $this->get_page();
  }

  function get_url() {
    return $this->url;
  }

  function set_html() {
    $curl = new Curl();
    $curl->get($this->url);
    if ($curl->error) {
      return 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
    } else {
      $this->html = $curl->response;
    }
  }
  function get_html() {
    return $this->html;
  }

  /*
   *
   * Todo: add other classes or methods to allow for more than article
   * not sure parsing that much regex is wise so html5 elements seem to be the
   * best option for this
   * possibly use: https://www.w3.org/People/Raggett/tidy/
   * This method assumes wordpress sites use
   *    <article></article>
   * for the content section
   *
   */

   function set_slug() {
     $url_parts = explode('/', $this->url);
     if(end($url_parts) == '') {
       array_pop($url_parts);
     }
     $this->slug = strlen(end($url_parts) > 5) ? end($url_parts) : 'Please-Replace-This-Slug';

   }

   function get_slug() {
     return $this->slug;
   }

  function set_content() {
    $pattern = '/<article(.+?)<\/article>/mis';
    if(preg_match($pattern, $this->html, $matches)) {
      $this->content = $matches[0];
    } else {
      $message = "There was no article for {$this->url}\n";
      $this->content = $message;
      echo $message;
    }
  }

  function get_content() {
    return $this->content;
  }

  function set_title() {
    $title_pattern = '/<title>(.+?)<\/title>/mis';
    if(preg_match($title_pattern, $this->html, $title)) {
      $title = $title[1];
    }
    $this->title = strlen($title) > 5 ? $title : 'This Post Had No Title';
  }

  function get_title() {
    return $this->title;
  }

  function set_meta_description() {
    $title_pattern = '/<meta name="description" content="(.+?)">/mis';
    if(preg_match($title_pattern, $this->html, $meta)) {
      $meta = $meta[1];
    }
    $this->meta = strlen($meta) > 5 ? $meta : "My Meta Description Brings No Leads To The Yard";
  }

  function get_meta_description() {
    return $this->meta;
  }

  function set_headline() {
    $headline_pattern = '/<h1(.+?)>(.+?)<\/h1>/mis';
    if(preg_match($headline_pattern, $this->html, $headline)) {
      $headline = $headline[2];
      if(preg_match('/<span>(.+?)<\/span>/mis', $headline, $headline)) {
        $headline = $headline[1];
      }
    } else {
      $headline_pattern = '/<h1>(.+?)<\/h1>/mis';
      if(preg_match($headline_pattern, $this->html, $headline)) {
        $headline = $headline[1];
      }
    }
    $this->headline = strlen($headline) > 5 ? $headline : 'My Headline Seems To Be Missing. Can You Help Me?';
  }
  function get_headline() {
    return $this->headline;
  }

  function download_images() {
    $path = $this->create_new_import_path();
    $this->images = $this->get_images_in_content();
    foreach($this->images as $image) {
      $image_name = $this->parse_image_name($image);
      array_push($this->imagenames, $image_name);
      $file_and_path = $path . $image_name;
      $curl = new Curl();
      $curl->download($image, $file_and_path);
    }
    return [ 'local_path' => $path, 'images' => $this->imagenames ];
  }

  function create_new_import_path() {
    $path = __DIR__ . '/import-' . date("Ymd").'/';
    if (!file_exists($path)) {
      mkdir($path, 0777, true);
    }
    return $path;
  }

  function parse_image_name($image) {
    $image_parts = explode('/', $image);
    if(end($image_parts) == '') {
      array_pop($image_parts);
    }
    return end($image_parts);
  }

  function get_images_in_content() {
    // https://stackoverflow.com/questions/18093990/php-regex-to-get-all-image-urls-on-the-page
    $pattern = "#https?://[^/\s]+/\S+\.(jpg|png|gif)#";
    // $image_pattern = '/https?:\/\/[^/\s]+/\S+\.(jpg|png|gif)/';
    // if(preg_match_all($image_pattern, $this->html, $images)) {
    if(preg_match_all($pattern, $this->content, $images)) {
      return array_unique($images[0]);
      // return $images;
    }
  }

  function replace_images_with_new($new_images) {
    for($i = 0; $i < count($this->images); $i++) {
      $this->content = str_replace($this->images[$i], $new_images[$i], $this->content);
    }
  }
}
?>

# README #

 This is a better implementation that is extendable and object oriented
 This setup allows you to get process an array of urls and take the:
            <article></article>
 and upload them into hubspot. The current setup here is a proof of concept
 using the HubSpot API and uploading to the Demo Hubspot Account.
 You can see the previous implementation in the repo or through another link
 After the three wordpress pages are uploaded you are able to paste your own
 wordpress page url and see how it goes into hubspot.
 Please note this is just a general template that we modify slightly.

Issues:
* Internal and external links: how to manage
* Images without http: Probably need to check SRC as the regex

### What is this repository for? ###

* This is where I'm going to be adding my hubspot api tests and ideas I decide to say is possible when I talk about it but never get around to actually doing it.

### How do I get set up? ###

Composer Needs
* php-curl-class/php-curl-class: https://github.com/php-curl-class/php-curl-class
* kint-php/kint: https://github.com/kint-php/kint


### Contribution guidelines ###

* I'm open to pull requests. 
* I'm open to code feedback
* I don't think I'm the best programmer. You don't have to be either. Together we can make something that works.

### Who do I talk to? ###

* Nicholas Decker: nickdeckerdevs AT gmail DOT com
* Your Therapist
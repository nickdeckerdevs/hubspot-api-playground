<?php


require __DIR__ . '/vendor/autoload.php';

use \Curl\Curl;
use \Kint\Kint;

// Kint::$enabled_mode = false; // Disable kint debugging by uncommenting this
?>
<pre>
/*
 * this is a quick and dirty way of grabbing content from a website page and
 * putting it into hubspot. The best way would be to (php) curl into each page,
 * parse the web page using a regular expression for the
 *             /&lt;article(*.?)&lt;\/article&gt;/
 * add them into your php object, then looping through that php object and
 * using the hubspot API to create and update the pages
 *
 * Ideally you could just loop through the XLS file (or csv if you prefer)
 * That way you could have some of the other data in the XLS file. But you
 * could also just copy a few of the columns out into a file and create a json
 * file to have that data in it... Really this is a matter of preference and
 * your end goal.
 *
 * https://developers.hubspot.com/docs/methods/pages/post_pages
 * Create your pages via the post_pages api endpoint. On your return, you will
 * parse the info to get your object id that was created.
 * Then publish the page! Automagicmania.
 * NOTE: This page may not work! [page is here for history]
 */
</pre>
<?php
$urls = [
  'http://www.relittle.com/walk-behind-riding-mowers/',
  'http://info.avalan.com/shop/aw900xtp-pair-900-mhz-outdoor-ethernet-panel-bridge/',
  'https://www.kaocollins.com/inktank/category/inspiration/'
];
$pages = [];
d($urls);
$id = 0;
foreach($urls as $url) {
  $title = '';
  $content = '';

  $curl = new Curl();
  $curl->get($url);

  if ($curl->error) {
    echo 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
  } else {
    $data = $curl->response;
    $pattern = '/<article(.+?)<\/article>/mis';
    if(preg_match($pattern, $data, $matches)) {
      $content = $matches[0];
      $title_pattern = '/<h1(.+?)>(.+?)<\/h1>/mis';
      if(preg_match($title_pattern, $content, $title)) {
        $title = $title[2];
        if(preg_match('/<span>(.+?)<\/span>/mis', $title, $final_title)) {
          $title = $final_title[1];
        }
      }
    } else {
      echo "There was no article for $url<br>";
    }
    $page_data = [
      'id' => $id,
      'original_url' => $url,
      'page_id' => '',
      'title' => $title,
      'content' => $content
    ];
    array_push($pages, $page_data);

  }
  $id++;

}
d($pages);
foreach($pages as $key => $page) {
  $create_new_page_endpoint = 'http://api.hubapi.com/content/api/v2/pages?hapikey=demo';
  $title = $pages[$key]['title'];
  /* added random number so you could run this multiple times with the same slug and not get an error */
  $slug = str_replace(' ', '-', $title) . '-' . rand(5, 1291223);;
  $body = [
    'name' => $title,
    'template_path' => 'hubspot_default/landing_page/basic_with_form/2_col_form_left.html','slug' => $slug,
    'html_title' => $title,
    'publish_immediately' => true,
    'widgets' => [
      'subheader' => [
        'body' => [
          'value' => $title
        ]
      ],
      'right_column' => [
        'body' => [
          'html' => $content
        ]
      ]
    ]
  ];
  $curl = new Curl();
  $curl->setHeader('Content-Type', 'application/json');
  $curl->post($create_new_page_endpoint, $body);
  d($curl->response);
  $page_id = $curl->response->id;
  echo "page_id: $page_id<br>";
  $pages[$key]['page_id'] = $page_id;

  if ($curl->error) {
      echo 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
  } else {
    echo 'check the page at: <a href="'.$curl->response->url.'" target="_blank">'.$curl->response->url.'</a><br>';
    /* publish the page for viewing pleasure */
    $publish = [ 'action' => 'schedule-publish' ];
    $curl = new Curl();
    $curl->setHeader('Content-Type', 'application/json');
    $curl->post("http://api.hubapi.com/content/api/v2/pages/{$page_id}/publish-action?hapikey=demo", $publish);
    d($curl->response);
  }
}
d($pages);

<?php
use \Curl\Curl;
use \Curl\MultiCurl;

class Hubspot {
  var $api_key;
  // var $portal_id;
  // var $base_url;

  function __construct($params = []) {
    $this->api_key = array_key_exists('api_key', $params) ? $params['api_key'] : 'demo';
    $this->portal_id = array_key_exists('portal_id', $params) ? $params['portal_id'] : false;
    $this->base_url = array_key_exists('base_url', $params) ? $params['base_url'] : 'http://api.hubapi.com';
  }

  function create_new_website_page($params) {
    $endpoint = "{$this->base_url}/content/api/v2/pages?hapikey={$this->api_key}";
    $curl = new Curl();
    $curl->setHeader('Content-Type', 'application/json');
    $curl->post($endpoint, $params['body']);
    if ($curl->error) {
      d($curl);
      $message = 'Website Page Creation Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
      echo $message;
      return $message;
    } else {
      $url = $curl->response->url;
      echo "View Hubspot api response:<br>";
      d($curl->response);
      if($params['publish']) {
        $page_id = $curl->response->id;
        $publish = [ 'action' => 'schedule-publish' ];
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->post("{$this->base_url}/content/api/v2/pages/{$page_id}/publish-action?hapikey=demo", $publish);
        if($curl->response == "") echo 'Check the page [id:'.$page_id.'] at: <a href="'.$url.'" target="_blank">'.$url.'</a><br>';
      }
      return $url;
    }
  }

  function upload_images($params) {
    $uploads = [];
    $endpoint = "{$this->base_url}/filemanager/api/v2/files?hapikey={$this->api_key}&overwrite=true";
    $folder = $this->create_folder($params['local_path']);
    foreach($params['images'] as $image) {
      $file_and_path = $params['local_path'].$image;
      $mime_type = mime_content_type($file_and_path);
      d($mime_type);
      $jpeg_file = new CURLFile($file_and_path, $mime_type, $image);
      $post_data = [
        "file" => $jpeg_file,
        "folder_id" => $folder['id'],
        "folder_paths" => $folder['name']
      ];
      $curl = new Curl();
      $curl->setHeader('Content-type', 'multipart/form-data');
      $curl->post($endpoint, $post_data);
      d($curl);
      if ($curl->error) {
        echo 'Image Upload Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
      } else {
        $new_url = $curl->response->objects[0]->url;
        array_push($uploads, $new_url);
      }
    }
    return $uploads;
  }

  function create_folder($path) {
    $path_parts = explode('/', $path);
    if(end($path_parts) == '') {
      array_pop($path_parts);
    }
    $folder_name = end($path_parts);
    $endpoint = "{$this->base_url}/filemanager/api/v2/folders?hapikey={$this->api_key}";
    $params = [
        'name' => $folder_name
    ];
    $curl = new Curl();
    $curl->setHeader('Content-Type', 'application/json');
    $curl->post($endpoint, $params);
    if ($curl->error) {
      if(!$curl->response->id) {
        echo 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
      }
    }
    return [ 'id' => $curl->response->id, 'name' => $folder_name ];
  }

}

?>
